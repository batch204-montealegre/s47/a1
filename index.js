
let firstName = document.querySelector('#txt-first-name');
let lastName = document.querySelector('#txt-last-name');
let fullName = document.querySelector('#span-full-name');



firstName.addEventListener('keyup', () => {
	update();
});

lastName.addEventListener('keyup', () => {
	update();
});


function update(){
	fullName.innerHTML = firstName.value + ' ' + lastName.value;
};